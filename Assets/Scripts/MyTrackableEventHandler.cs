﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyTrackableEventHandler : DefaultTrackableEventHandler
{

    public ITrackingListener TrackingListener;
       

    protected override void OnTrackingFound()
    {
        base.OnTrackingFound();

        //animationDelegate(true);
        if(TrackingListener != null)
            TrackingListener.TrackingChanged(true);
        //Animator.SetBool("enter", true);
    }

    protected override void OnTrackingLost()
    {
        base.OnTrackingFound();

        //animationDelegate(false);
        if (TrackingListener != null)
            TrackingListener.TrackingChanged(false);
        //Animator.SetBool("enter", false);


    }
}

﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Video;
using Vuforia;

public class ImageTargetPlayAnimationBack : MonoBehaviour, ITrackableEventHandler
{
    public Animator VideoAnimator;
    public VideoPlayer VideoPlayer;

    private TrackableBehaviour mTrackableBehaviour;


    void Start()
    {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
        //StartCoroutine(StartCor());
    }

    public void OnTrackableStateChanged(
                                    TrackableBehaviour.Status previousStatus,
                                    TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            Debug.Log("Animator.Play(buttonsEnter)");
            VideoAnimator.SetBool("enter", true);
            VideoPlayer.Play();
        }
        else
        {
            Debug.Log("Animator.Play(buttonsExit)");
            VideoAnimator.SetBool("enter", false);
            VideoPlayer.Stop();
        }
    }

    /*bool e;
    bool _enter
    {
        get
        {
            return e;
        }
        set
        {
            if(value != e)
            {
                e = value;
                ButtonAnimator.SetBool("enter", e);
            }
        }
    }
    IEnumerator StartCor()
    {
        while (true)
        {
            yield return new WaitForSeconds(2f);
            _enter = !_enter;
        }
    }*/
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BusinessCard : MonoBehaviour
{
    public GameObject OverCardCanvasPrefab;

    BusinessCardFront _front = null;
    BusinessCardBack _back = null;
    GameObject _overCardCanvas = null;

    #region data
    public enum Side
    {
        FRONT,
        BACK
    }
    public class CardImage
    {
        public int pixel_width;
        public int pixel_height;
    }
    class Data
    {
        public Side side;
        public CardImage card_image_data;
        public FrontData front;
        public BackData back;
    }
    class FrontData
    {
        public string email;
        public string phone;
        public string website;
        public string profilePicture;
        public string google_map_image;
        public string google_map_url;
    }
    class BackData
    {
        //public string animation;
        //public string video_url;
    }
    Data DeserializeData(string dataString)
    {
        Data data = null;
        try
        {
            data = JsonConvert.DeserializeObject<Data>(dataString);
        }
        catch (Exception e)
        {
            Debug.Log("BusinessCard DeserializeData error " + e.ToString());
        }
        return data;
    }
    #endregion

    #region test
    void TestData()
    {
        Data data1 = new Data();
        data1.side = Side.FRONT;
        data1.card_image_data = new CardImage();
        data1.card_image_data.pixel_width = 406;
        data1.card_image_data.pixel_height = 232;
        data1.front = new FrontData();
        data1.front.email = "karadjordje@spinview.io";
        data1.front.phone = "+381658003466";
        data1.front.website = "https://www.linkedin.com/in/karadjordje-petrovic-40364589/";

        string serializedData1 = JsonConvert.SerializeObject(data1);

        /*Data data2 = new Data();
        data2.side = Side.BACK;
        data2.card_image_data = new CardImage();
        data2.card_image_data.pixel_width = 406;
        data2.card_image_data.pixel_height = 232;
        data2.back = new BackData();
        data2.back.video_url = "https://www.youtube.com/watch?v=Jq98OEXJSr8";

        string serializedData2 = JsonConvert.SerializeObject(data2);*/
    }
    #endregion

    public void Activate(string metaData)
    {
        //TestData();
        Data data = DeserializeData(metaData);
        if (data == null) return;

        if (_overCardCanvas != null) Destroy(_overCardCanvas);

        _overCardCanvas = Instantiate(OverCardCanvasPrefab, transform);
        _overCardCanvas.transform.localPosition = new Vector3(0f, 0.01f, 0f);
        _overCardCanvas.transform.localScale = Vector3.one * 0.01f;
        _overCardCanvas.transform.localEulerAngles = new Vector3(90f, 0f, 0f);

        _front = _overCardCanvas.GetComponentInChildren<BusinessCardFront>();
        _back = _overCardCanvas.GetComponentInChildren<BusinessCardBack>();

        //activate side:
        switch (data.side)
        {
            case Side.FRONT:
                _front.Root.SetActive(true);
                _back.Root.SetActive(false);
                _front.Activate(data.card_image_data.pixel_width, data.card_image_data.pixel_height,
                    data.front.email, data.front.phone, data.front.website, data.front.profilePicture, data.front.google_map_url, data.front.google_map_image);
                GetComponent<MyTrackableEventHandler>().TrackingListener = _front;
                break;
            case Side.BACK:
                _front.Root.SetActive(false);
                _back.Root.SetActive(true);
                //back.Activate(data.card_image_data.pixel_width, data.card_image_data.pixel_height, data.back.video_url);
                GetComponent<MyTrackableEventHandler>().TrackingListener = _back;
                break;
        }
    }

    #region back
    public Button BackButton;
    void LoadMainScene()
    {
        Application.Quit();
    }
    private void Awake()
    {
        BackButton.onClick.AddListener(LoadMainScene);
        //test//StartCoroutine(DownloadImage("https://www.dropbox.com/s/vdeh921zqek2yg0/Birgitta%20Bokstrom.jpg?raw=1"));
    }
    #endregion
}

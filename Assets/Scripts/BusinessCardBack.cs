﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public interface ITrackingListener
{
    void TrackingChanged(bool value);
}
public class BusinessCardBack : MonoBehaviour, ITrackingListener
{

    public GameObject Root;
    public RectTransform MarkerOverRect;
    public RectTransform VideoRect;
    public RectTransform VideoMaskRect;
    //public Animator Animator;
    public VideoPlayer VideoPlayer;

    enum VideoAnimatorState
    {
        HIDDEN,
        ENTERING,
        EXITING,
        SHOWN
    }
    VideoAnimatorState _currentVideoState;
    
    public void Activate(int card_width, int card_height, string url)
    {
        float videoHeight = (float)card_height / (float)card_width * 100f;
        MarkerOverRect.sizeDelta = new Vector2(100f, videoHeight);
        VideoRect.sizeDelta = new Vector2(100f, videoHeight);
        VideoMaskRect.sizeDelta = new Vector2(100f, videoHeight);
        //TODO: VideoRect size should size of video, and not marker

        _hiddenPosition = Vector2.zero;
        _shownPosition = new Vector2(0f, -videoHeight);

        if (_tracking)
        {
            _currentVideoState = VideoAnimatorState.ENTERING;
            _startAnimationTime = Time.time;
        }
        else
        {
            _currentVideoState = VideoAnimatorState.HIDDEN;
        }
        StartCoroutine(CustomAnimation());

        //Animator.SetBool("enter", true);

        VideoPlayer.Play();
    }

    Vector2 _hiddenPosition;
    Vector2 _shownPosition;
    float _startAnimationTime = 0f;
    IEnumerator CustomAnimation()
    {
        while (true)
        {
            switch (_currentVideoState)
            {
                case VideoAnimatorState.HIDDEN:
                case VideoAnimatorState.SHOWN:
                    break;
                case VideoAnimatorState.ENTERING:
                    VideoRect.anchoredPosition = Vector2.Lerp(_hiddenPosition, _shownPosition, (Time.time - _startAnimationTime) / 0.5f);
                    
                    if (Time.time - _startAnimationTime > 0.5f)
                        _currentVideoState = VideoAnimatorState.SHOWN;
                    break;
                case VideoAnimatorState.EXITING:
                    VideoRect.anchoredPosition = Vector2.Lerp(_shownPosition, _hiddenPosition, (Time.time - _startAnimationTime) / 0.5f);
                    if (Time.time - _startAnimationTime > 0.5f)
                        _currentVideoState = VideoAnimatorState.HIDDEN;
                    break;
            }
            OpenCompanySiteRect.position = VideoRect.position;
            OpenCompanySiteRect.sizeDelta = VideoRect.sizeDelta;
            yield return null;
        }
    }

    bool _tracking = false;
    public void TrackingChanged(bool value)
    {
        if (_tracking && !value)
        {
            _currentVideoState = VideoAnimatorState.EXITING;
            _startAnimationTime = Time.time;
        }
        else if (!_tracking && value)
        {
            _currentVideoState = VideoAnimatorState.ENTERING;
            _startAnimationTime = Time.time;
        }
        _tracking = value;

        Root.SetActive(_tracking);
    }

    public Button OpenCompanySiteButton;
    public RectTransform OpenCompanySiteRect;

    void Awake()
    {
        OpenCompanySiteButton.onClick.AddListener(OpenCompanySite);
    }
    void OpenCompanySite()
    {
        if (_tracking)
            Application.OpenURL("https://spinview.io/");
    }
}

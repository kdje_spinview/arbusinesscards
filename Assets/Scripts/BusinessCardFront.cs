﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;


public class BusinessCardFront : MonoBehaviour, ITrackingListener
{
    enum ButtonType
    {
        EmailButton,
        PhoneButton,
        WebButton
    }

    public GameObject Root;
    public Animator AnimatorButtons;
    public Animator AnimatorMap;
    public Animator AnimatorProfileImage;
    public RectTransform MarkerOverRect;


    string _email;
    string _phone;
    string _website;
    string _map;

    float ar_card_width;
    float ar_card_height;
    public void Activate(int card_width, int card_height, string email, string phone, string web, string profilePicture, string map, string map_image)
    {
        ar_card_width = (float)card_width / (float)card_height * 100f;
        ar_card_height = 100f;
        MarkerOverRect.sizeDelta = new Vector2(ar_card_width, ar_card_height);

        _email = email;
        _phone = phone;
        _website = web;
        _map = map;

        AnimatorButtons.SetBool("enter", true);
        //ButtonsRoot.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, (card_width - card_height) / 2);
        //ButtonsRoot.SetActive(true);

        /*testing*/
        //profilePicture = "https://www.dropbox.com/s/epn74miah1slbbi/ZELJKO.jpeg?raw=1";
        if (!string.IsNullOrEmpty(profilePicture))
            StartCoroutine(DownloadProfileImage(profilePicture, card_width, card_height));
        /*testing*/
        //_map = "https://www.google.com/maps/place/LABS+House/@51.5176594,-0.125728,17z/data=!3m1!4b1!4m5!3m4!1s0x48761b96f733b88d:0xb0ff5f2d1655c274!8m2!3d51.5176561!4d-0.123534";// "http://maps.google.com/maps?q=London";
        //map_image = "https://www.map_image.com/s/epn74miah1slbbi/ZELJKO.jpeg?raw=1";
        //map_image = "https://www.dropbox.com/s/9o0hgrsmcvsz2ok/map123.PNG?raw=1";
        if (!string.IsNullOrEmpty(map_image))
            StartCoroutine(DownloadMapImage(map_image, card_width, card_height));
    }

    public Button MailButton;
    public Button PhoneButton;
    public Button WebButton;

    public Button MapButton;

    void Awake()
    {
        MailButton.onClick.AddListener(OpenMail);
        PhoneButton.onClick.AddListener(Call);
        WebButton.onClick.AddListener(OpenWebSite);

        MapButton.onClick.AddListener(OpenMap);
    }
    void OpenMail()
    {
        if(_tracking)
            Application.OpenURL("mailto:" + _email + "?subject=Email&body=from AR Business Card");
    }
    void Call()
    {
        if (_tracking)
            Application.OpenURL("tel://" + _phone);
    }
    void OpenWebSite()
    {
        if (_tracking)
            Application.OpenURL(_website);
    }
    void OpenMap()
    {
        Application.OpenURL(_map);
    }

    bool _tracking;
    public void TrackingChanged(bool value)
    {
        Debug.Log("TrackingChanged to " + value);
        if (_tracking == value) return;

        _tracking = value;

        if (_tracking)
        {
            AnimatorButtons.SetBool("enter", value);
            if (_mapImageDownloaded) AnimatorMap.SetBool("enter", value);
            if (_profileImageDownloaded)
            {
                MarkerOverRect.gameObject.SetActive(true);
                AnimatorProfileImage.SetBool("enter", value);
            }
        }
        else
        {
            MarkerOverRect.gameObject.SetActive(false);
        }
    }

    #region profile_picture
    public RawImage UserImage;
    public RectTransform UserImageRect;
    public RectTransform UserMaskRect;


    bool _profileImageDownloaded = false;
    IEnumerator DownloadProfileImage(string url, float card_width, float card_height)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
            Debug.Log(request.error);
        else
        {
            UserMaskRect.anchoredPosition = new Vector2((-40f/ 284) * ar_card_width, (37f / 453f) * ar_card_height);

            Texture downloadedTexture = ((DownloadHandlerTexture)request.downloadHandler).texture;
            UserImage.texture = downloadedTexture;
            
            UserMaskRect.sizeDelta = Vector2.one * ar_card_width * (129.4f / 284f);//(downloadedTexture.height < downloadedTexture.width ? 1f : downloadedTexture.height / downloadedTexture.width);
            
            UserMaskRect.GetComponent<Image>().enabled = true;
            UserImage.gameObject.SetActive(true);
            _profileImageDownloaded = true;
            AnimatorProfileImage.SetBool("enter", true);
        }
    }
    #endregion

    #region map_image
    public RawImage MapImage;
    public RectTransform MapImageRect;

    bool _mapImageDownloaded = false;
    IEnumerator DownloadMapImage(string url, float card_width, float card_height)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
            Debug.Log(request.error);
        else
        {
            _mapImageDownloaded = true;
            AnimatorMap.SetBool("enter", true);
            Texture downloadedTexture = ((DownloadHandlerTexture)request.downloadHandler).texture;
            MapImage.texture = downloadedTexture;
            MapImage.GetComponent<RectTransform>().sizeDelta = new Vector2(100f, 100f * downloadedTexture.height / downloadedTexture.width);

            MapImageRect.sizeDelta = Vector2.one * 100f * (downloadedTexture.height < downloadedTexture.width ? 1f : downloadedTexture.height / downloadedTexture.width);
        }
    }
    #endregion
}

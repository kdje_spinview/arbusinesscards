﻿using System;
using System.Collections;
using UnityEngine;
using Vuforia;

public class ImageTargetPlayAnimationFront : MonoBehaviour, ITrackableEventHandler
{
    public Animator ButtonAnimator;
    private TrackableBehaviour mTrackableBehaviour;


    void Start()
    {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
        //StartCoroutine(StartCor());
    }

    public void OnTrackableStateChanged(
                                    TrackableBehaviour.Status previousStatus,
                                    TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            Debug.Log("Animator.Play(buttonsEnter)");
            ButtonAnimator.SetBool("enter", true);
        }
        else
        {
            Debug.Log("Animator.Play(buttonsExit)");
            ButtonAnimator.SetBool("enter", false);
        }
    }

    /*bool e;
    bool _enter
    {
        get
        {
            return e;
        }
        set
        {
            if(value != e)
            {
                e = value;
                ButtonAnimator.SetBool("enter", e);
            }
        }
    }
    IEnumerator StartCor()
    {
        while (true)
        {
            yield return new WaitForSeconds(2f);
            _enter = !_enter;
        }
    }*/
}